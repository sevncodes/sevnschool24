#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <limits.h>
#include <float.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <time.h>

/* Physical constants. */
#define IC_GRAVITATIONAL_CONSTANT_CGS 6.67430e-8 /* Newtonian constant of gravitation G. */
#define IC_SOLAR_MASS_CGS 1.989e33 /* Browse reference. */
#define IC_YEAR_CGS 3.1471982e7 /* Seconds in a sidereal year. */
#define IC_YEAR_DAYS 365.256363004 /* Days in a sidereal year. */
#define IC_SOLAR_RADIUS_CGS 6.957e10 /* Solar radius in cm. */

/* Data type flags for converting parameter values. */
#define IC_LINE 0 /* Complete line. */
#define IC_INTEGER 1 /* Integer. */
#define IC_LONG_INTEGER 2 /* Long integer. */
#define IC_UNSIGNED 3 /* Unsigned integer. */
#define IC_LONG_UNSIGNED 4 /* Long unsigned integer. */
#define IC_REAL 5 /* Double-precision floating point. */
#define IC_CHARACTER 6 /* Character. */
#define IC_STRING 7 /* String of characters. */

/* Distributions */
#define IC_MASS_DIST_COUNT 2 /* Primary mass distributions. */
#define IC_MASS_POWER_LAW 0
#define IC_MASS_KROUPA 1
#define IC_Q_DIST_COUNT 2 /* Mass-ratio (q) distributions. */
#define IC_Q_POWER_LAW 0
#define IC_Q_POWER_LAW_M2CUT 1
#define IC_PERIOD_DIST_COUNT 1 /* Period distributions. */
#define IC_PERIOD_POWER_LAW 0
#define IC_ECC_DIST_COUNT 1 /* Eccentricity distributions. */
#define IC_ECC_POWER_LAW 0

/* System types */
#define IC_STAR "star"
#define IC_BINARY "binary"

/* Administrative stuff. */
#define IC_NAME_LENGTH 200 /* Maximum length for file names. */
#define IC_SHORT_NAME_LENGTH 50 /* Maximum length for input file names. */
#define IC_MESSAGE_LENGTH 1000 /* Maximum length for messages. */
#define IC_LINE_LENGTH 10000 /* Lines in a file */
#define IC_RED "\x1b[31;1m" /* Red. */
#define IC_COLOR_RESET "\x1b[0m" /* Standard. */
#define OZ_YELLOW "\x1b[33;1m" /* Yellow. */

/* Useful macros. */
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))

/* Auxiliar methods. */
unsigned long GetRandomSeed();
double ComputeSemimajorRSUN(double, double, double);
double PowerLawDeviate(double, double, double);
void ReadParameterFile();
void ReadPrimaryMass();
void ReadPrimaryMetallicity();
void ReadPrimaryAngVel();
void ReadPrimarySNModel();
void ReadPrimaryInitialTime();
void ReadMassRatio();
void ReadSecondaryMetallicity();
void ReadSecondaryAngVel();
void ReadSecondarySNModel();
void ReadSecondaryInitialTime();
void ReadPeriod();
void ReadEccentricity();
void ReadFinalTime();
void ReadOutputTimes();
int GetParameter(FILE *, char *, int, void *, int);
void DisplayMessage(char *, FILE *);
void DisplayError(char *, FILE *);
void DisplayAbortMessage();
void AbortProgram();

/* General global variables. */
char Messages[IC_MESSAGE_LENGTH]; /* Messages. */
gsl_rng *RandomNumberGenerator;
unsigned long RandomSeed;
FILE *ParameterFile; /* Parameter file. */
FILE *OutputFile; /* Output file. */
FILE *LogFile; /* Log file. */
char ParameterFileName[IC_NAME_LENGTH]; /* Parameter file name. */
char OutputFileName[IC_NAME_LENGTH]; /* Output file name. */
char LogFileName[IC_NAME_LENGTH]; /* Log file name. */
const char ExtensionLog[4] = ".log";
const char ExtensionOutput[4] = ".dat";

/* Distribution options. */
const char MassDistributionName[2][IC_NAME_LENGTH] = {"PowerLaw", "Kroupa"}; /* Kroupa not implemented yet. */
const char MassRatioDistributionName[2][IC_NAME_LENGTH] = {"PowerLaw", "PowerLawM2Cut"};
const char MassRatioMinOption[IC_NAME_LENGTH] = {"M2Cut"};
const char PeriodDistributionName[1][IC_NAME_LENGTH] = {"LogPowerLaw"};
const char EccentricityDistributionName[1][IC_NAME_LENGTH] = {"PowerLaw"};

/* Global variables related to distributions. */
unsigned NSystems;
char SystemType[IC_SHORT_NAME_LENGTH];
double MassMinMSUN, MassMaxMSUN, MassIndex, LogPeriodMinDAYS, LogPeriodMaxDAYS, LogPeriodIndex, EccMin, EccMax, EccIndex;
double MassRatioMin, MassRatioMax, MassRatioMinM2MSUN, MassRatioIndex;
double ZPrim, ZSec, OmegaPrim, OmegaSec;
char SNModelPrim[IC_SHORT_NAME_LENGTH], SNModelSec[IC_SHORT_NAME_LENGTH], InitialTimePrim[IC_SHORT_NAME_LENGTH], InitialTimeSec[IC_SHORT_NAME_LENGTH], FinalTime[IC_SHORT_NAME_LENGTH], OutputDT[IC_SHORT_NAME_LENGTH];

int main(int argc, char **argv)
{
  unsigned long i;
  double primary_mass_MSUN, min_mass_ratio, companion_mass_MSUN, mass_ratio, log_period_DAYS, period_DAYS, ecc, semi_RSUN, total_mass;

  if (argc < 3)
  {
    DisplayError("Argument(s) missing.", stdout);
    DisplayMessage("Usage: <sics exec> <input file> <output files base name>", stdout);
    DisplayAbortMessage();
    AbortProgram();
  }

  ParameterFile = fopen(argv[1], "rb");
  sprintf(OutputFileName, "%s.ic", argv[2]);
  sprintf(LogFileName, "%s.log", argv[2]);
  OutputFile = fopen(OutputFileName, "w");
  LogFile = fopen(LogFileName, "w");
  ReadParameterFile();

  RandomNumberGenerator = gsl_rng_alloc(gsl_rng_r250);
  RandomSeed = GetRandomSeed();
  gsl_rng_set(RandomNumberGenerator, RandomSeed);

  total_mass = 0.0;
  if (!strcmp(SystemType, IC_STAR))
  {
    for (i = 0; i < NSystems; i++)
    {
      primary_mass_MSUN = PowerLawDeviate(MassMinMSUN, MassMaxMSUN, -MassIndex);
      fprintf(OutputFile, "%le %le %le %s %s %s %s\n", primary_mass_MSUN, ZPrim, OmegaPrim, SNModelPrim, InitialTimePrim, FinalTime, OutputDT);
      total_mass += primary_mass_MSUN;
    }
  }
  else if (!strcmp(SystemType, IC_BINARY))
  {
    for (i = 0; i < NSystems; i++)
    {
      primary_mass_MSUN = PowerLawDeviate(MassMinMSUN, MassMaxMSUN, -MassIndex);
      min_mass_ratio = MAX(MassRatioMin, MassRatioMinM2MSUN / primary_mass_MSUN);
      mass_ratio = PowerLawDeviate(min_mass_ratio, MassRatioMax, -MassRatioIndex);
      companion_mass_MSUN = mass_ratio * primary_mass_MSUN;
      log_period_DAYS = PowerLawDeviate(LogPeriodMinDAYS, LogPeriodMaxDAYS, -LogPeriodIndex);
      period_DAYS = pow(10.0, log_period_DAYS);
      ecc = PowerLawDeviate(EccMin, EccMax, -EccIndex);
      semi_RSUN = ComputeSemimajorRSUN(period_DAYS, primary_mass_MSUN, companion_mass_MSUN);
      fprintf(OutputFile, "%le %le %le %s %s %le %le %le %s %s %le %le %s %s\n", 
                        primary_mass_MSUN, ZPrim, OmegaPrim, SNModelPrim, InitialTimePrim, 
                        companion_mass_MSUN, ZSec, OmegaSec, SNModelSec, InitialTimeSec, 
                        semi_RSUN, ecc, FinalTime, OutputDT);
      total_mass += primary_mass_MSUN + companion_mass_MSUN;
    }
  }
  

  fclose(ParameterFile);
  fclose(OutputFile);

  /* Report success. */

  printf("Total mass is %le Msun.\n", total_mass);
  fprintf(LogFile, "Total mass is %le Msun.\n", total_mass);
  DisplayMessage("Program run successfully.", LogFile);
  DisplayMessage("", stdout);
  DisplayMessage("Program run successfully.", stdout);
  DisplayMessage("", stdout);
  sprintf(Messages, "Output file is '%s'. Log file is: '%s'.", OutputFileName, LogFileName);
  DisplayMessage(Messages, stdout);
  
  return 0;
}

double ComputeSemimajorRSUN(double period_days, double m1_msun, double m2_msun)
{
  double period_yr, period_cgs, total_mass_cgs, semi_cgs;
  period_yr = period_days / IC_YEAR_DAYS;
  period_cgs = period_yr * IC_YEAR_CGS;
  total_mass_cgs = (m1_msun + m2_msun) * IC_SOLAR_MASS_CGS;
  semi_cgs = pow(period_cgs * period_cgs * total_mass_cgs * IC_GRAVITATIONAL_CONSTANT_CGS / (4.0 * M_PI * M_PI), 1.0/3.0);
  return semi_cgs / IC_SOLAR_RADIUS_CGS;
}

double PowerLawDeviate(double min, double max, double index)
{
  double deviate, std_deviate;

  std_deviate = gsl_rng_uniform(RandomNumberGenerator);
  if (index == -1.0)
    deviate = min * exp(std_deviate * log(max / min)); /* Change variables to obtain power-law deviate. */
  else if (index == 0.0)
    deviate = min + (max - min) * std_deviate;
  else
  {
    index += 1.0;
    min = pow(min, index);
    max = pow(max, index);
    deviate = pow(min + (max - min) * std_deviate, 1.0 / index);
  }
  return deviate;
}

void ReadParameterFile()
{
  unsigned n_systems;
  char key[IC_NAME_LENGTH];

  /* Get keywords from parameter file and assign global variables. */
  if (GetParameter(ParameterFile, "NumberSystems", IC_LONG_UNSIGNED, &n_systems, 1) != EXIT_SUCCESS)
  {
      sprintf(Messages, "Invalid NumberSystems or NumberSystems key not found.");
      DisplayError(Messages, stdout);
      DisplayAbortMessage();
      AbortProgram();
  }
  NSystems = n_systems;
  if (GetParameter(ParameterFile, "SystemType", IC_STRING, key, 0) != EXIT_SUCCESS)
  {
      sprintf(Messages, "Invalid SystemType or SystemType key not found.");
      DisplayError(Messages, stdout);
      DisplayAbortMessage();
      AbortProgram();
  }
  strcpy(SystemType, key);
  if (strcmp(SystemType, IC_BINARY) != 0 && strcmp(SystemType, IC_STAR) != 0)
  {
    sprintf(Messages, "Invalid SystemType. Given: '%s'. Options: 'star', 'binary'.", SystemType);
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }

  sprintf(Messages, "Model includes %d %s systems", NSystems, SystemType);
  DisplayMessage(Messages, LogFile);
  ReadPrimaryMass();
  ReadPrimaryMetallicity();
  ReadPrimaryAngVel();
  ReadPrimarySNModel();
  ReadPrimaryInitialTime();
  if (!strcmp(SystemType, IC_BINARY))
  {
    ReadMassRatio();
    ReadSecondaryMetallicity();
    ReadSecondaryAngVel();
    ReadSecondarySNModel();
    ReadSecondaryInitialTime();
    ReadPeriod();
    ReadEccentricity();
  }
  ReadFinalTime();
  ReadOutputTimes();

  /* Report success. */
  DisplayMessage("Input file read successfully.", LogFile);
  DisplayMessage("", LogFile);
}

void ReadPrimaryMass()
{
  int errors;
  unsigned i, rewind;
  char key[IC_NAME_LENGTH];
  double min_mass, max_mass, index;

  rewind = 1;

  if (GetParameter(ParameterFile, "MassDistribution", IC_STRING, key, rewind) != EXIT_SUCCESS)
  {
    DisplayError("No primary-mass model defined.", stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  rewind = 0; /* Read next keyword from the current file position, to avoid reading the first one indefinitely. */

  /* Identify keyword with a valid mass-distribution type name. */

  for (i = 0; i < IC_MASS_DIST_COUNT; i++)
    if (!strcmp(key, MassDistributionName[i]))
      break;
  if (i == IC_MASS_DIST_COUNT)
  {
    sprintf(Messages, "Invalid MassDistribution");
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  sprintf(Messages, "Model includes primary mass distribution '%s'.", MassDistributionName[i]);
  DisplayMessage(Messages, LogFile);

  /* Search for particle type parameters. */

  errors = GetParameter(ParameterFile, "MassMin", IC_REAL, &min_mass, rewind);
  if (errors == EXIT_FAILURE || min_mass <= 0.0)
  {
    sprintf(Messages, "Invalid minimum mass.");
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  MassMinMSUN = min_mass;
  sprintf(Messages, "Minimum mass is %.3e Msun.", min_mass);
  DisplayMessage(Messages, LogFile);
  errors = GetParameter(ParameterFile, "MassMax", IC_REAL, &max_mass, rewind);
  if (errors == EXIT_FAILURE || max_mass < min_mass)
  {
    sprintf(Messages, "Invalid maximum mass.");
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  MassMaxMSUN = max_mass;
  sprintf(Messages, "Maximum mass is %.3e Msun.", max_mass);
  DisplayMessage(Messages, LogFile);
  errors = GetParameter(ParameterFile, "MassIndex", IC_REAL, &index, rewind);
  if (errors == EXIT_FAILURE)
  {
    sprintf(Messages, "Invalid mass index.");
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  MassIndex = index;
  sprintf(Messages, "Mass index is %.3e.", index);
  DisplayMessage(Messages, LogFile);
}
  
void ReadPrimaryMetallicity()
{
  double zprim;
  int errors, rewind;

  rewind = 1;

  errors = GetParameter(ParameterFile, "ZPrim", IC_REAL, &zprim, rewind);
  if (errors == EXIT_FAILURE || zprim <= 0.0)
  {
    sprintf(Messages, "Invalid primary metallicity.");
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  ZPrim = zprim;
  sprintf(Messages, "Metallicity of primaries is %.3e.", zprim);
  DisplayMessage(Messages, LogFile);
}
  
void ReadPrimaryAngVel()
{
  double omegaprim;
  int errors, rewind;

  rewind = 1;

  errors = GetParameter(ParameterFile, "OmegaPrim", IC_REAL, &omegaprim, rewind);
  if (errors == EXIT_FAILURE || omegaprim < 0.0)
  {
    sprintf(Messages, "Invalid primary angular velocity.");
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  OmegaPrim = omegaprim;
  sprintf(Messages, "Normalized angular velocity of primaries is %.3e.", omegaprim);
  DisplayMessage(Messages, LogFile);
}
  
void ReadPrimarySNModel()
{
  char key[IC_NAME_LENGTH];
  int errors, rewind;

  rewind = 1;

  errors = GetParameter(ParameterFile, "SNModelPrim", IC_STRING, key, rewind);
  if (errors == EXIT_FAILURE)
  {
    sprintf(Messages, "Invalid SN model of primaries.");
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  strcpy(SNModelPrim, key);
  sprintf(Messages, "Supernova model of primaries is '%s'.", SNModelPrim);
  DisplayMessage(Messages, LogFile);
}
  
void ReadPrimaryInitialTime()
{
  char key[IC_NAME_LENGTH];
  int errors, rewind;

  rewind = 1;

  errors = GetParameter(ParameterFile, "InitialTimePrim", IC_STRING, key, rewind);
  if (errors == EXIT_FAILURE)
  {
    sprintf(Messages, "Invalid initial time of primaries.");
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  strcpy(InitialTimePrim, key);
  sprintf(Messages, "Initial time of primaries is '%s'.", InitialTimePrim);
  DisplayMessage(Messages, LogFile);
}

void ReadMassRatio()
{
  int errors;
  unsigned i, rewind;
  char key[IC_NAME_LENGTH];
  double min_q, max_q, index, m2_min;

  rewind = 1;

  if (GetParameter(ParameterFile, "MassRatioDistribution", IC_STRING, key, rewind) != EXIT_SUCCESS)
  {
    DisplayError("No mass-ratio model defined.", stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  rewind = 0; /* Read next keyword from the current file position, to avoid reading the first one indefinitely. */

  /* Identify keyword with a valid period-distribution type name. */

  for (i = 0; i < IC_Q_DIST_COUNT; i++)
    if (!strcmp(key, MassRatioDistributionName[i]))
      break;
  if (i == IC_Q_DIST_COUNT)
  {
    sprintf(Messages, "Invalid MassRatioDistribution");
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  sprintf(Messages, "Model includes mass-ratio distribution '%s'.", MassRatioDistributionName[i]);
  DisplayMessage(Messages, LogFile);

  /* Search for particle type parameters. */

  errors = GetParameter(ParameterFile, "MassRatioMin", IC_REAL, &min_q, rewind);
  if (errors == EXIT_FAILURE)
  {
    sprintf(Messages, "Invalid minimum mass ratio.");
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  MassRatioMin = min_q;
  sprintf(Messages, "Minimum mass ratio is %s.", MassRatioMinOption);
  DisplayMessage(Messages, LogFile);
  errors = GetParameter(ParameterFile, "SecMassMin", IC_REAL, &m2_min, rewind);
  if (errors == EXIT_FAILURE || m2_min <= 0.0)
  {
    sprintf(Messages, "Invalid secondary mass minimum for mass ratio.");
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  MassRatioMinM2MSUN = m2_min; /* Min mass ratio depends on current primary mass. Prepares variable to compute minimum mass ratio later at main. */
  sprintf(Messages, "Minimum secondary mass is %.3e Msun.", m2_min);
  DisplayMessage(Messages, LogFile);
  errors = GetParameter(ParameterFile, "MassRatioMax", IC_REAL, &max_q, rewind);
  if (errors == EXIT_FAILURE)
  {
    sprintf(Messages, "Invalid maximum mass ratio.");
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  MassRatioMax = max_q;
  sprintf(Messages, "Maximum mass ratio is %.3e.", max_q);
  DisplayMessage(Messages, LogFile);
  errors = GetParameter(ParameterFile, "MassRatioIndex", IC_REAL, &index, rewind);
  if (errors == EXIT_FAILURE)
  {
    sprintf(Messages, "Invalid mass ratio index.");
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  MassRatioIndex = index;
  sprintf(Messages, "Mass ratio index is %.3e.", index);
  DisplayMessage(Messages, LogFile);
}

void ReadSecondaryMetallicity()
{
  double zsec;
  int errors, rewind;

  rewind = 1;

  errors = GetParameter(ParameterFile, "ZSec", IC_REAL, &zsec, rewind);
  if (errors == EXIT_FAILURE || zsec <= 0.0)
  {
    sprintf(Messages, "Invalid secondary Metallicity.");
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  ZSec = zsec;
  sprintf(Messages, "Metallicity of secondaries is %.3e.", zsec);
  DisplayMessage(Messages, LogFile);
}
    
void ReadSecondaryAngVel()
{
  double omegasec;
  int errors, rewind;

  rewind = 1;

  errors = GetParameter(ParameterFile, "OmegaPrim", IC_REAL, &omegasec, rewind);
  if (errors == EXIT_FAILURE || omegasec < 0.0)
  {
    sprintf(Messages, "Invalid angular velocity of secondaries.");
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  OmegaSec = omegasec;
  sprintf(Messages, "Normalized angular velocity of secondaries is %.3e.", omegasec);
  DisplayMessage(Messages, LogFile);
}
    
void ReadSecondarySNModel()
{
  char key[IC_NAME_LENGTH];
  int errors, rewind;

  rewind = 1;

  errors = GetParameter(ParameterFile, "SNModelSec", IC_STRING, key, rewind);
  if (errors == EXIT_FAILURE)
  {
    sprintf(Messages, "Invalid SN model of secondaries.");
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  strcpy(SNModelSec, key);
  sprintf(Messages, "Supernova model of secondaries is '%s'.", SNModelSec);
  DisplayMessage(Messages, LogFile);
}
    
void ReadSecondaryInitialTime()
{
  char key[IC_NAME_LENGTH];
  int errors, rewind;

  rewind = 1;

  errors = GetParameter(ParameterFile, "InitialTimeSec", IC_STRING, key, rewind);
  if (errors == EXIT_FAILURE)
  {
    sprintf(Messages, "Invalid initial time of secondaries.");
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  strcpy(InitialTimeSec, key);
  sprintf(Messages, "Initial time of secondaries is '%s'.", InitialTimeSec);
  DisplayMessage(Messages, LogFile);
}
    
void ReadPeriod()
{
  int errors;
  unsigned i, rewind;
  char key[IC_NAME_LENGTH];
  double min_period, max_period, index;

  rewind = 1;

  if (GetParameter(ParameterFile, "PeriodDistribution", IC_STRING, key, rewind) != EXIT_SUCCESS)
  {
    DisplayError("No period model defined.", stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  rewind = 0; /* Read next keyword from the current file position, to avoid reading the first one indefinitely. */

  /* Identify keyword with a valid period-distribution type name. */

  for (i = 0; i < IC_PERIOD_DIST_COUNT; i++)
    if (!strcmp(key, PeriodDistributionName[i]))
      break;
  if (i == IC_PERIOD_DIST_COUNT)
  {
    sprintf(Messages, "Invalid PeriodDistribution");
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  sprintf(Messages, "Model includes period distribution '%s'.", PeriodDistributionName[i]);
  DisplayMessage(Messages, LogFile);

  /* Search for particle type parameters. */

  errors = GetParameter(ParameterFile, "PeriodMin", IC_REAL, &min_period, rewind);
  if (errors == EXIT_FAILURE || min_period <= 0.0)
  {
    sprintf(Messages, "Invalid minimum period.");
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  LogPeriodMinDAYS = min_period;
  sprintf(Messages, "Minimum period is %.3e d.", min_period);
  DisplayMessage(Messages, LogFile);
  errors = GetParameter(ParameterFile, "PeriodMax", IC_REAL, &max_period, rewind);
  if (errors == EXIT_FAILURE || max_period < min_period)
  {
    sprintf(Messages, "Invalid maximum period.");
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  LogPeriodMaxDAYS = max_period;
  sprintf(Messages, "Maximum period is %.3e d.", max_period);
  DisplayMessage(Messages, LogFile);
  errors = GetParameter(ParameterFile, "PeriodIndex", IC_REAL, &index, rewind);
  if (errors == EXIT_FAILURE)
  {
    sprintf(Messages, "Invalid period index.");
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  LogPeriodIndex = index;
  sprintf(Messages, "Period index is %.3e.", index);
  DisplayMessage(Messages, LogFile);
}

void ReadEccentricity()
{
  int errors;
  unsigned i, rewind;
  char key[IC_NAME_LENGTH];
  double min_ecc, max_ecc, index;

  rewind = 1;

  if (GetParameter(ParameterFile, "EccentricityDistribution", IC_STRING, key, rewind) != EXIT_SUCCESS)
  {
    DisplayError("No eccentricity model defined.", stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  rewind = 0; /* Read next keyword from the current file position, to avoid reading the first one indefinitely. */

  /* Identify keyword with a valid period-distribution type name. */

  for (i = 0; i < IC_ECC_DIST_COUNT; i++)
    if (!strcmp(key, EccentricityDistributionName[i]))
      break;
  if (i == IC_ECC_DIST_COUNT)
  {
    sprintf(Messages, "Invalid EccentricityDistribution");
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  sprintf(Messages, "Model includes eccentricity model '%s'.", EccentricityDistributionName[i]);
  DisplayMessage(Messages, LogFile);

  /* Search for particle type parameters. */

  errors = GetParameter(ParameterFile, "EccMin", IC_REAL, &min_ecc, rewind);
  if (errors == EXIT_FAILURE || min_ecc <= 0.0)
  {
    sprintf(Messages, "Invalid minimum eccentricity.");
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  EccMin = min_ecc;
  sprintf(Messages, "Minimum eccentricity is %.3e d.", min_ecc);
  DisplayMessage(Messages, LogFile);
  errors = GetParameter(ParameterFile, "EccMax", IC_REAL, &max_ecc, rewind);
  if (errors == EXIT_FAILURE || max_ecc < min_ecc)
  {
    sprintf(Messages, "Invalid maximum eccentricity.");
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  EccMax = max_ecc;
  sprintf(Messages, "Maximum eccentricity is %.3e d.", max_ecc);
  DisplayMessage(Messages, LogFile);
  errors = GetParameter(ParameterFile, "EccIndex", IC_REAL, &index, rewind);
  if (errors == EXIT_FAILURE)
  {
    sprintf(Messages, "Invalid ecc index.");
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  EccIndex = index;
  sprintf(Messages, "Ecc index is %.3e.", index);
  DisplayMessage(Messages, LogFile);
}

void ReadFinalTime()
{
  char key[IC_NAME_LENGTH];
  int errors, rewind;

  rewind = 1;

  errors = GetParameter(ParameterFile, "FinalTime", IC_STRING, key, rewind);
  if (errors == EXIT_FAILURE)
  {
    sprintf(Messages, "Invalid final time.");
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  strcpy(FinalTime, key);
  sprintf(Messages, "Final time is %s.", FinalTime);
  DisplayMessage(Messages, LogFile);
}

void ReadOutputTimes()
{
  char key[IC_NAME_LENGTH];
  int errors, rewind;

  rewind = 1;

  errors = GetParameter(ParameterFile, "OutputTimes", IC_STRING, key, rewind);
  if (errors == EXIT_FAILURE)
  {
    sprintf(Messages, "Invalid output times.");
    DisplayError(Messages, stdout);
    DisplayAbortMessage();
    AbortProgram();
  }
  strcpy(OutputDT, key);
  sprintf(Messages, "Output dt is %s.", OutputDT);
  DisplayMessage(Messages, LogFile);
}


/** GetParameter.

    - Parses a stream searching for a supplied parameter keyword, and reads the associated value (assumed to have the supplied type).
    - Parameters in the stream must have the format [keyword] [value] (no blanks in [keyword]).
    - [value] must have the format defined by 'type', and may contain a trailing comment of any kind, separated from the formatted value.
    - The stream is read from its current reading position if 'rewind' is null, otherwise it is read from its beginning.
    - Returns EXIT_SUCCESS (EXIT_FAILURE) on success (failure). **/

int GetParameter(FILE *stream, char *keyword, int type, void *value, int rewind)
{
  char line[IC_LINE_LENGTH], key[IC_LINE_LENGTH], val[IC_LINE_LENGTH];

  if (rewind)
    fseek(stream, 0, SEEK_SET);
  while (!feof(stream))
  {
    /* Read a line. */

    if (fscanf(stream, "%[^\n]\n", line) < 1)
      return EXIT_FAILURE;

    /* Get a keyword and value from the line. */

    if (sscanf(line, "%s %[^\n]", key, val) < 2)
      continue;

    /* If the line contains the desired parameter, convert it to type 'type'. */

    if (!strcmp(key, keyword))
    {
      switch (type)
      {
        case IC_INTEGER: /* Signed integer. */
          sscanf(val, "%i", (int *)value);
          break;
        case IC_LONG_INTEGER: /* Signed long integer. */
          sscanf(val, "%li", (long *)value);
          break;
        case IC_UNSIGNED: /* Unsigned integer. */
          sscanf(val, "%u", (unsigned *)value);
          break;
        case IC_LONG_UNSIGNED: /* Long unsigned integer. */
          sscanf(val, "%lu", (unsigned long *)value);
          break;
        case IC_REAL: /* Real (double precision floating point). */
          sscanf(val, "%lg", (double *)value);
          break;
        case IC_CHARACTER: /* Single character. */
          *((char *)value) = val[0];
          break;
        case IC_STRING: /* Character string. */
          sscanf(val, "%s", (char *)value);
          break;
        case IC_LINE: /* Complete line. */
        default:
          strcpy((char *)value, val);
          break;
      }
      return EXIT_SUCCESS;
    }
  }
  return EXIT_FAILURE;
}

/* GetRandomSeed.

   - Provides a random seed by reading garbage from /dev/urandom. */

unsigned long GetRandomSeed()
{
  FILE *random_file;
  unsigned long seed;
  int error;

  random_file = fopen("/dev/urandom", "rb");
  if (!random_file)
    return 0;
  error = fread(&seed, 1, sizeof(unsigned long), random_file);
  if (error != sizeof(unsigned long)) /* Not enough garbage in file. */
  {
    fclose(random_file);
    return 0;
  }
  fclose(random_file);
  return seed;
}

/** DisplayMessage.

    - Prints a message to a stream, adding a final '\n' character. **/

void DisplayMessage(char *message, FILE *stream)
{
  fprintf(stream, "%s\n", message);
  fflush(stream);
}

/** DisplayError.

    - Prints an error message to a stream. **/

void DisplayError(char *message, FILE *stream)
{
  if (stream == stdout)
    fprintf(stream, IC_RED "ERROR: " IC_COLOR_RESET "%s\n", message);
  else
    fprintf(stream, "ERROR: %s\n", message);
  fflush(stream);
}

/** DisplayAbortMessage.

    - Prints the message 'OneZone aborted' to stdout. **/

void DisplayAbortMessage()
{
  fprintf(stdout, IC_RED "Program aborted. " IC_COLOR_RESET "See stdout for errors.\n\n");
  fflush(stdout);
}

/** AbortProgram.

    - Aborts the program. **/

void AbortProgram()
{
  exit(EXIT_SUCCESS);
}