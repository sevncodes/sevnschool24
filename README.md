# 1st. SEVN School on Binary Population Synthesis

This the public repository for the 1st. SEVN School on Binary Population Synthesis held at IAFE (Institute of Astronomy and Space Physics), Universidad of Buenos Aires.
The course is organised by Giuliano Iorio ([giuliano.iorio.astro@gmail.com](giuliano.iorio.astro@gmail.com)) and Gaston J. Escobar ([gje245@gmail.com](gje245@gmail.com)) thanks to the support of [CUIA](https://www.cuia.net/2024/04/30/14-edicion-de-las-jornadas-del-cuia-en-argentina/6886/), [DEMOBLACK](http://demoblack.com), and [TEONGRAV](https://web.infn.it/CSN4/index.php/it/17-esperimenti/195-teongrav-home).
 
## Resources 
This repository will contain all the resources (slides/files) used during the school.
In addition there some external resources:

- **SEVN main paper:** [Iorio et al., 2023, MNRAS, 524, 426](https://ui.adsabs.harvard.edu/abs/2023MNRAS.524..426I/abstract). The paper contains the detailed descriptions of how single stellar evolution and binary processes are implemented in SEVN. It also contains a first scientific exploration regarding the study of binary compact objects. 
- **SEVN gitlab repository:** [https://gitlab.com/sevncodes/sevn](https://gitlab.com/sevncodes/sevn). This the public SEVN repository where you can find the code and additional resources as the userguide (included also in this repository) and bash and python scripts to faciliate the execution of SEVN and the analysis of its output.
- **Zenodo repository of Iorio23:** [https://zenodo.org/records/7794546](https://zenodo.org/records/7794546) this public Zenodo repository contains all the necessary data to reproduce the results of the [Iorio+23](https://ui.adsabs.harvard.edu/abs/2023MNRAS.524..426I/abstract) paper, including initial conditions and run scripts.
- **SEVNpy documentation:** [http://sevn.rtfd.io/](http://sevn.rtfd.io/) this website contains the documentation for the Python version of SEVN (currently under development, but still fully usable, especially for reading the logfiles).
- **SEVNpy on Colab tutorial:** In case of problems in installing SEVNpy, it is possibile to get and install it directly in [Google Colab](https://colab.research.google.com/).  A video tutorial on how to use SEVNpy in Colab is available at this link: [https://youtu.be/xFDtGcHVrLA](https://youtu.be/xFDtGcHVrLA)
- **TOPCAT**, topcat is a software (writtein in Java) that is very useful to give a first look at tabular data as the ones produce by SEVN. Give a look to the website: https://www.star.bris.ac.uk/~mbt/topcat/
